package visitor;

import state.Charizard;
import state.Charmander;
import state.Charmeleon;
import state.State;


public class ConcreteVisitor implements Visitor {

	@Override
	public State visit(Charmander tilaolio) {
		return Charmeleon.getInstance();
	}

	@Override
	public State visit(Charmeleon tilaolio) {
		// TODO Auto-generated method stub
		return Charizard.getInstance();
	}

	@Override
	public State visit(Charizard tilaolio) {
		// TODO Auto-generated method stub
		return Charizard.getInstance();
	}



}
