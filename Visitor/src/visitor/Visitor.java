package visitor;

import state.Charizard;
import state.Charmander;
import state.Charmeleon;
import state.State;


public interface Visitor {
	public State visit(Charmander tilaolio);
	public State visit(Charmeleon tilaolio);
	public State visit(Charizard tilaolio);

}
