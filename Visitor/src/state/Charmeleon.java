package state;

import visitor.ConcreteVisitor;
import visitor.Visitor;

public class Charmeleon implements State {
	
	private final String name = "Charmeleon";
	private Visitor visitor = new ConcreteVisitor();
	
	private static volatile Charmeleon INSTANCE = null;
	
	public static Charmeleon getInstance(){
		if(INSTANCE == null){
			INSTANCE = new Charmeleon();
		}
		return INSTANCE;
	}

	@Override
	public String getName(Pokemon s) {
		// TODO Auto-generated method stub
		return name;
	}

	@Override
	public void changeState(Pokemon s) {
		s.changeState(visitor.visit(this));
		
	}

	@Override
	public void fight(Pokemon s) {
		System.out.println("R�YH");
		s.changeState(Charizard.getInstance());
	}

	@Override
	public void move(Pokemon s) {
		System.out.println("k�pk�p");
		
	}
	



}
