package state;

import visitor.ConcreteVisitor;
import visitor.Visitor;

public class Charizard implements State{

	private final String name = "Charizard";
	private Visitor visitor = new ConcreteVisitor();
	private static volatile Charizard INSTANCE = null;
	
	public static Charizard getInstance(){
		if(INSTANCE == null){
			INSTANCE = new Charizard();
		}
		return INSTANCE;
	}

	@Override
	public String getName(Pokemon s) {
		// TODO Auto-generated method stub
		return name;
	}

	@Override
	public void changeState(Pokemon s) {
		s.changeState(visitor.visit(this));
		
	}

	@Override
	public void fight(Pokemon s) {
		System.out.println("Supertaistelu");
		
	}

	@Override
	public void move(Pokemon s) {
		System.out.println("Superkävely");
		
	}


}
