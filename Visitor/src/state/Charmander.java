package state;

import visitor.ConcreteVisitor;
import visitor.Visitor;

public class Charmander implements State {

	private final String name = "Charmander";
	private Visitor visitor = new ConcreteVisitor();
	private static volatile Charmander INSTANCE = null;
	
	public static Charmander getInstance(){
		if(INSTANCE == null){
			INSTANCE = new Charmander();
		}
		return INSTANCE;
	}

	@Override
	public String getName(Pokemon s) {
		// TODO Auto-generated method stub
		return name;
	}

	@Override
	public void changeState(Pokemon s) {
		s.changeState(visitor.visit(this));
		
	}

	@Override
	public void fight(Pokemon s) {
		System.out.println("Tuli-isku");
		s.changeState(Charmeleon.getInstance());
		
	}

	@Override
	public void move(Pokemon s) {
		System.out.println("Liiku hiljaa");
		
	}
	


}
