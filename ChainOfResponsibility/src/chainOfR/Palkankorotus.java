package chainOfR;

public abstract class Palkankorotus {

	protected static final double prosentti = 100;
	protected Palkankorotus successor;
	
	public void setSuccessor(Palkankorotus successor){
		this.successor = successor;
	}
	
	public abstract void processRequest(KorotusRequest request);
	
}
