package chainOfR;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class Client {
	public static void main(String[] args) {
		Lähiesimies lähiesimies = new Lähiesimies();
		Päällikkö päällikkö = new Päällikkö();
		Toimitusjohtaja toimitusjohtaja = new Toimitusjohtaja();

		lähiesimies.setSuccessor(päällikkö);
		päällikkö.setSuccessor(toimitusjohtaja);

		try {
			while (true) { 
				System.out.println("- - - - - - - - - - - - - - - - - - - -");
				System.out.println();
				System.out.println("Anna palkankorotuspyyntö prosentteina");
				System.out.print("$ ");

				double prosentti = Double.parseDouble(new BufferedReader(new InputStreamReader(System.in)).readLine());

				lähiesimies.processRequest(new KorotusRequest(prosentti));
				
				System.out.println();
			}
		} catch (Exception e) {
		
			System.out.println("Sovellus suljettu.");
			
		}

	}

}
