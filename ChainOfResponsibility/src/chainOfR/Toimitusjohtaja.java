package chainOfR;

public class Toimitusjohtaja extends Palkankorotus {

	private final double ALLOWABLE = prosentti;
	
	@Override
	public void processRequest(KorotusRequest request) {
		if (request.getM��r�() > 0.5 && request.getM��r�() <= ALLOWABLE){
			System.out.println("Toimitusjohtaja hyv�ksyy " + request.getM��r�() + "% korotuksen.");
		} else {
			System.out.println("Palkankorotuspyynt� ylitt�� 100%. Kukaan ei pysty hyv�ksym��n t�t�!");
		}
		
	}

}
