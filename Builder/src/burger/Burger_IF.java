package burger;

public interface Burger_IF {
	
	public void addItem(Ingredient_IF ingredient);
	
	public double getCost();
	
	public void printIngredients();

}
