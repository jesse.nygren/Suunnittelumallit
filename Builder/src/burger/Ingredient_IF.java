package burger;

public interface Ingredient_IF {

	public String getIngredient();
	
	public double getPrice();
	
}
