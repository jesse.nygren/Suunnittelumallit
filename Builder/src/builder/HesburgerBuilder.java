package builder;

import burger.Burger_IF;
import ingredients.Juusto;
import ingredients.Pihvi;
import ingredients.Suolakurkku;
import ingredients.S�mpyl�;
import ingredients.Tomaatti;

public class HesburgerBuilder implements Builder_IF {
	
	Burger_IF kerroshamppari;
	
	public HesburgerBuilder(){
		kerroshamppari = new HeseBurger();
	}
	
	@Override
	public Burger_IF buildBurger() {
		
		kerroshamppari.addItem(new S�mpyl�());
		kerroshamppari.addItem(new Pihvi());
		kerroshamppari.addItem(new Pihvi());
		kerroshamppari.addItem(new Suolakurkku());
		kerroshamppari.addItem(new Tomaatti());
		kerroshamppari.addItem(new Juusto());
		return kerroshamppari;
	}

	@Override
	public Burger_IF getBurger() {
		// TODO Auto-generated method stub
		return kerroshamppari;
	}



}
