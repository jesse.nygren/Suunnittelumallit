package builder;

import java.util.ArrayList;
import java.util.List;

import burger.Burger_IF;
import burger.Ingredient_IF;

public class HeseBurger implements Burger_IF {

	private List<Ingredient_IF> ingredients = new ArrayList<Ingredient_IF>();
	
	@Override
	public void addItem(Ingredient_IF ingredient) {
		ingredients.add(ingredient);
		
	}

	@Override
	public double getCost() {
		
		double price = 0;
		
		for (Ingredient_IF i : ingredients){
			price += i.getPrice();
		}
		
		return price;
	}

	@Override
	public void printIngredients() {
		
		System.out.print("Ingredients: ");
		
		for (Ingredient_IF i : ingredients){
			
			System.out.print(i.getIngredient() + " ");
		}
		
	}

}
