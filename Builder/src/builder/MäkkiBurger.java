package builder;

import burger.Burger_IF;
import burger.Ingredient_IF;

public class MäkkiBurger implements Burger_IF {

	private StringBuilder sb = new StringBuilder();
	private Double cost = 0.0;
	
	@Override
	public void addItem(Ingredient_IF ingredient) {
		
		if (sb.length() == 0){
			sb.append("Ingredients: ");
		}
		
		sb.append(ingredient.getIngredient() + " ");
		
		cost += ingredient.getPrice();
		
	}

	@Override
	public double getCost() {
		
		return Math.round(cost);
	}

	@Override
	public void printIngredients() {
		System.out.println(sb);
		
	}

}
