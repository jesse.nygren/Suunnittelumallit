package builder;

import burger.Burger_IF;

public interface Builder_IF {

	public Burger_IF buildBurger();
	
	public Burger_IF getBurger();
	
}
