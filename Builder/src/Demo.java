import builder.Builder_IF;
import builder.HesburgerBuilder;
import builder.McDonaldsBuilder;
import burger.Burger_IF;

public class Demo {

	public static void main(String[] args) {
		
		Builder_IF mäkkäri = new McDonaldsBuilder();
		Builder_IF hese = new HesburgerBuilder();
		
		
		
		Burger_IF hesenBursa = hese.buildBurger();
		Burger_IF mäkkäriPurilainen = mäkkäri.buildBurger();
		
		// List
		System.out.println("Hesen purilainen");
		hesenBursa.printIngredients();
		System.out.println();
		System.out.println("Hinta: " + hesenBursa.getCost());
		System.out.println();
		
		
		// Stringbuilder
		System.out.println("Mäkkärin purilainen");
		mäkkäriPurilainen.printIngredients();
		System.out.println("Hinta: " + mäkkäriPurilainen.getCost());
	
		
		
		
		
	}
	
}
