package memento;

public class Demo {
	public static void main(String[] args) {

		ArvuuttajaOriginator arvuuttaja = new ArvuuttajaOriginator();
		
	
		for (int i = 0; i < 10; i++){
			AsiakasCaretaker asiakas = new AsiakasCaretaker(arvuuttaja);
			Thread t = new Thread(asiakas);
			t.start();
		}
		
	}
}
