package memento;

public class ArvuuttajaOriginator {

	private int randomNumber;
	
	public void setRandomNumber(int randomNumber){
		this.randomNumber = randomNumber;
	}
	
	public int getRandomNumber(){
		return randomNumber;
	}
	
	public Memento saveRandomNumberToMemento(){
		return new Memento(randomNumber);
	}
	
	public void getRandomNumberFromMemento(Memento memento){
		randomNumber = memento.getRandomNumber();
	}
	
	public Memento liityPeliin(){
		this.randomNumber = (int)(Math.random() * 10 + 1);
		return saveRandomNumberToMemento();
	}
	
	public boolean vertaaArvaus(Memento memento, int arvaus){
		
		if (memento.getRandomNumber() == arvaus){
			return true;
		} else {
			return false;
		}
		
	}
	
}
