package memento;

import java.util.ArrayList;
import java.util.List;

public class AsiakasCaretaker implements Runnable {

	private Memento memento;
	private ArvuuttajaOriginator arvuuttaja;
	
	public AsiakasCaretaker(ArvuuttajaOriginator arvuuttaja){
		this.arvuuttaja = arvuuttaja;
	}
	

	public boolean arvaaNumero(){
		int random = (int)(Math.random() * 10 + 1);
	

		return arvuuttaja.vertaaArvaus(memento, random);
	}

	public void liityPeliin(){
		memento = this.arvuuttaja.liityPeliin();
		System.out.println("Liitetty");
	}
	
	@Override
	public void run() {
		liityPeliin();
		
		
		while(!arvaaNumero()){
			System.out.println("Arvasit v��rin");
			
			
		}
		System.out.println("Oikea vastaus");
	}
	
	
}
