package memento;

public class Memento {

	private int randomNumber;
	
	public Memento(int randomNumber){
		this.randomNumber = randomNumber;
	}
	
	public int getRandomNumber(){
		return randomNumber;
	}
	
}
