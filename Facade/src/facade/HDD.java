package facade;

public class HDD {

	
	private byte[] hdd;
	
	public HDD() {
		hdd  = new byte[] {12,42,53,64,34,43,31,53};
	}
	
	public byte[] read(long sector, int size) {
		
		
		return hdd;
	}

	public void displayData() {
		
		for(byte b : hdd) {
			System.out.println(b);
		}
		
	}

}
