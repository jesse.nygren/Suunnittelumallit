package facade;

public class RAM {
	
	byte[] ram;
	
	public RAM() {
		ram = new byte[8];
	}

	public byte[] load(long position, byte[] data) {
		
		for (int i = 0; i<ram.length;i++) {
			ram[i] = data[i];
		}
		
		return ram;
	}
	
	public void displayMemorySlots(byte[] ram) {
		
		for (int i = 0; i<ram.length;i++) {
			System.out.println("SLOT_" + i +": " + ram[i]);
		}
		
	}

	
	
}
