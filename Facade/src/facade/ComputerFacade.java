package facade;

public class ComputerFacade {

	private CPU cpu;
	private RAM ram;
	private HDD hdd;
	
	private long BOOT_ADDRESS = 2;
	private long BOOT_SECTOR = 3;
	private int size = 2;
	
	public ComputerFacade() {
		this.cpu = new CPU();
		this.ram = new RAM();
		this.hdd = new HDD();
	}
	
	public void start() {
		cpu.freeze();
		ram.displayMemorySlots(ram.load(BOOT_ADDRESS, hdd.read(BOOT_SECTOR, size)));
		cpu.jump(BOOT_ADDRESS);
		cpu.execute();
	}
	
}
