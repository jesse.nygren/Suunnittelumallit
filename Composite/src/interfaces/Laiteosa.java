package interfaces;

import java.util.ArrayList;

public interface Laiteosa {
	public abstract void addComponent(Laiteosa osa) throws Exception;
	public abstract void removeComponent(int index) throws Exception;
	public abstract Laiteosa getComponent(int index) throws Exception;
	public abstract int getComponentCount() throws Exception;
	public abstract double getSystemHinta() throws Exception;
	public abstract double getHinta();
	public abstract ArrayList<Laiteosa> getKomponentit() throws Exception;
}
