package composite;

import java.util.ArrayList;

import interfaces.Laiteosa;


public class Kotelo implements Laiteosa{

	private double hinta;
	private ArrayList<Laiteosa>komponentit;
	
	public Kotelo(double hinta){
		this.hinta = hinta;
		komponentit = new ArrayList<Laiteosa>();
	}
	
	@Override
	public double getHinta() {
		// TODO Auto-generated method stub
		return hinta;
	}

	@Override
	public void addComponent(Laiteosa osa) {
		komponentit.add(osa);
		
	}

	@Override
	public void removeComponent(int index) {
		komponentit.remove(index);
		
	}
	@Override
	public Laiteosa getComponent(int index) {
		// TODO Auto-generated method stub
		return komponentit.get(index);
	}

	@Override
	public int getComponentCount() {
		// TODO Auto-generated method stub
		return komponentit.size();
	}
	@Override
	public double getSystemHinta() throws Exception {
		double totalHinta = hinta;
		
		for(Laiteosa osa : komponentit){
			
			for(int i = 0; i < osa.getKomponentit().size();i++){
				ArrayList<Laiteosa> k = osa.getKomponentit();
				totalHinta = totalHinta + k.get(i).getHinta();
				
				
			}
			
			totalHinta = totalHinta + osa.getHinta();
			
		}
		return totalHinta;
	}
	
	@Override
	public ArrayList<Laiteosa> getKomponentit() {
		return komponentit;
	}

}
