package composite;

import java.util.ArrayList;

import interfaces.Laiteosa;


public class Emolevy implements Laiteosa {

	private double hinta;
	private ArrayList<Laiteosa>komponentit;

	@Override
	public ArrayList<Laiteosa> getKomponentit() {
		return komponentit;
	}

	public Emolevy(double hinta){
		this.hinta = hinta;
		komponentit = new ArrayList<Laiteosa>();
	}
	
	@Override
	public double getHinta() {
		// TODO Auto-generated method stub
		return hinta;
	}


	@Override
	public void addComponent(Laiteosa osa) {
		komponentit.add(osa);
		
	}


	@Override
	public void removeComponent(int index) {
		komponentit.remove(index);
		
	}

	@Override
	public Laiteosa getComponent(int index) {
		// TODO Auto-generated method stub
		return komponentit.get(index);
	}

	@Override
	public int getComponentCount() {
		// TODO Auto-generated method stub
		return komponentit.size();
	}

	@Override
	public double getSystemHinta() {
		double totalHinta = hinta;

		
		for (Laiteosa osa : komponentit){
			
			
			totalHinta = totalHinta + osa.getHinta();
		}
		
		return totalHinta;
	}
	
}
