package main;

import composite.Emolevy;
import composite.Kotelo;
import interfaces.Laiteosa;

import leaf.Muistipiiri;
import leaf.Prosessori;
import leaf.Verkkokortti;

public class Main {

	public static void main(String[] args) {
		Laiteosa kotelo = new Kotelo(100);
		
		Laiteosa emo = new Emolevy(150);
		
		Laiteosa prossu = new Prosessori(200);
		
		Laiteosa ram = new Muistipiiri(50);
		
		Laiteosa verkko = new Verkkokortti(99);
		
		
		
		try {
			kotelo.addComponent(emo);
			
			emo.addComponent(prossu);
			emo.addComponent(ram);
			emo.addComponent(verkko);
			
			System.out.println(kotelo.getHinta());
			System.out.println(kotelo.getSystemHinta());
			System.out.println(emo.getSystemHinta());
			System.out.println(emo.getComponent(2).getClass());
			System.out.println(emo.getComponent(2).getHinta());
			kotelo.removeComponent(0);
			System.out.println(kotelo.getSystemHinta());
			
			
		} catch (Exception e) {
			System.out.println("Something went wrong");
			e.printStackTrace();
		}
	}
	
}
