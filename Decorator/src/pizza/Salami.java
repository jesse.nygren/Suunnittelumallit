package pizza;

public class Salami extends PizzaDecorator {

	private double hinta = 1.50;
	
	public Salami(Pizza_IF pizzaToBeDecorated) {
		super(pizzaToBeDecorated);
		// TODO Auto-generated constructor stub
	}
	
	@Override
	public String getDescription() {
		//
		return super.getDescription() + ", Salami";
	}
	
	@Override 
	public double getHinta(){
		return super.getHinta() + hinta;
	}
	
}
