package pizza;

public class Herkkusieni extends PizzaDecorator {

	private double hinta = 1.50;
	
	public Herkkusieni(Pizza_IF pizzaToBeDecorated) {
		super(pizzaToBeDecorated);
		// TODO Auto-generated constructor stub
	}

	@Override
	public String getDescription() {
		//
		return super.getDescription() + ", Herkkusieni";
	}
	
	@Override 
	public double getHinta(){
		return super.getHinta() + hinta;
	}
	
}
