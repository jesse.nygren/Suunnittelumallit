package pizza;

public class Tomaattikastike extends PizzaDecorator {

	private double hinta = 2;
	
	public Tomaattikastike(Pizza_IF pizzaToBeDecorated) {
		super(pizzaToBeDecorated);
		// TODO Auto-generated constructor stub
	}
	
	@Override
	public String getDescription() {
		//
		return super.getDescription() + ", Tomaattikastike";
	}
	
	@Override 
	public double getHinta(){
		return super.getHinta() + hinta;
	}


}
