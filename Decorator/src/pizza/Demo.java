package pizza;

public class Demo {
	
	public static void main(String[] args) {
		
		// Luodaan pizzat
		Pizza_IF peruspizza = new Juusto(new Tomaattikastike(new Pizzapohja()));
		Pizza_IF salamiylläri = new Salami(new Ananas(new Juusto(new Tomaattikastike(new Pizzapohja()))));
		Pizza_IF vegespecial = new Herkkusieni(new Ananas(new Juusto(new Tomaattikastike(new Pizzapohja()))));
		// Tulostetaan tiedot
		System.out.println(peruspizza.getDescription() + " " + peruspizza.getHinta() + "€");
		System.out.println(salamiylläri.getDescription() + " " + salamiylläri.getHinta() + "€");
		System.out.println(vegespecial.getDescription() + " " + vegespecial.getHinta() + "€");
		
		
		
	}

	
	

}
