package pizza;

public class Ananas extends PizzaDecorator {
	
	private double hinta = 0.90;

	public Ananas(Pizza_IF pizzaToBeDecorated) {
		super(pizzaToBeDecorated);
		// TODO Auto-generated constructor stub
	}

	@Override
	public String getDescription() {
		//
		return super.getDescription() + ", Ananas";
	}
	
	@Override 
	public double getHinta(){
		return super.getHinta() + hinta;
	}
	
	
}
