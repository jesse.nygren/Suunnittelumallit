package pizza;

public class Juusto extends PizzaDecorator {

	private double hinta = 0.75;
	
	public Juusto(Pizza_IF pizzaToBeDecorated) {
		super(pizzaToBeDecorated);
		// TODO Auto-generated constructor stub
	}

	@Override
	public String getDescription() {
		//
		return super.getDescription() + ", Juusto";
	}
	
	@Override 
	public double getHinta(){
		return super.getHinta() + hinta;
	}
	
	
}
