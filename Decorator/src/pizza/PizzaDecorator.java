package pizza;

public class PizzaDecorator implements Pizza_IF {

	protected Pizza_IF pizzaToBeDecorated;
	
	
	public PizzaDecorator(Pizza_IF pizzaToBeDecorated){
		this.pizzaToBeDecorated = pizzaToBeDecorated;
	}
	
	
	public String getDescription() {
		// Delegoidaan seuraavalle
		return pizzaToBeDecorated.getDescription();
	}


	@Override
	public double getHinta() {
		// Delegoidaan seuraavalle
		return pizzaToBeDecorated.getHinta();
	}

}
