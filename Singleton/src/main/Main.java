package main;

import factory.KoneInsinööriTehdas;
import interfaces.Insinööritehdas;

public class Main {
	public static void main(String[] args) {

		Insinööritehdas tehdas = KoneInsinööriTehdas.getInstance();
		
		System.out.println(tehdas.createInsinööri());

		
	}
}
