package factory;

import interfaces.Insinööri;
import interfaces.Insinööritehdas;
import product.KoneInsinööri;

public class KoneInsinööriTehdas implements Insinööritehdas {

	private KoneInsinööriTehdas(){};
	
	private static volatile KoneInsinööriTehdas INSTANCE = null;

	
	public static KoneInsinööriTehdas getInstance() {
		
		if (INSTANCE == null){
			INSTANCE = new KoneInsinööriTehdas();
		}
		
		return INSTANCE;
	}
	
	
	@Override
	public Insinööri createInsinööri() {

		return new KoneInsinööri();
	}


}
