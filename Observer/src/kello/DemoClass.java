package kello;

public class DemoClass {
	public static void main(String[] args) {

		ClockTimer ct = new ClockTimer();
		
		DigitalClock dc = new DigitalClock(ct);
		
		new Thread(ct).start();
		
		dc.update(ct, ct);
		
	}
}
