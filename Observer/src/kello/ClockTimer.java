package kello;

import java.time.Clock;
import java.time.LocalDateTime;
import java.util.Observable;


public class ClockTimer extends Observable implements Runnable {

	private int hour, minute, second;
	private int oldSec;
	
	public int getHour() {
		return hour;
	}

	public int getMinute() {
		return minute;
	}

	public int getSecond() {
		return second;
	}

	@Override
	public void run() {
		try {
			tick();
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	void tick() throws InterruptedException {
		while (true) {
			
			LocalDateTime now = LocalDateTime.now();
			
			hour = now.getHour();
			minute = now.getMinute();
			second =  now.getSecond();
			setChanged();
			notifyObservers(this);

			Thread.sleep(1000);
		}
	}

}
