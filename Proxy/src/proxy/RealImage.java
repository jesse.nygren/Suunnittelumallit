package proxy;

public class RealImage implements Image {
	
	private String filename = null;

	
	public RealImage(final String filename){
		this.filename = filename;
	
	
	}
	
	private void loadImageFromDisk(){
		System.out.println("Loading: " + filename);
	}
	
	public void displayImage(){
		System.out.println("Displaying: " + filename);
		loadImageFromDisk();
	}

	@Override
	public void showData() {
		System.out.println("Imagedata: " + filename);
		
	}
	
}
