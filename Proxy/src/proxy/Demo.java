package proxy;

import java.util.ArrayList;
import java.util.List;

public class Demo {

	public static void main(String[] args) {
		List<Image> kuvakansio = new ArrayList<Image>();
		
		kuvakansio.add(new ProxyImage("Kissa"));
		kuvakansio.add(new ProxyImage("Koira"));
		kuvakansio.add(new ProxyImage("Hevonen"));
	

		System.out.println("N�ytet��n data ennen kuvan latausta");
		System.out.println();
		
		kuvakansio.get(0).showData();
		kuvakansio.get(0).displayImage();
		System.out.println();
		System.out.println("N�ytet��n vain data t�ss�");
		
		kuvakansio.get(1).showData();
		//kuvakansio.get(1).displayImage();
		
		
		System.out.println();
		kuvakansio.get(2).showData();
		kuvakansio.get(2).displayImage();
		
	
		
		
		
		
	}
}
