package state;

public class Pokemon {

	private State state;
	
	
	public Pokemon(){
		state = Charmander.getInstance();
	}
	
	public String getName() {

		return state.getName(this);
	}


	protected void changeState(State s) {
		this.state = s;
	}


	public void fight() {
		state.fight(this);
		
	}


	public void move() {
		state.move(this);
		
	}
}
