package state;

public interface State {

	abstract String getName(Pokemon s);
	
	abstract void changeState(Pokemon s);
	
	abstract void fight(Pokemon s);
	
	abstract void move(Pokemon s);
	
	
}
