package state;

public class Demo {
	
	public static void main(String[] args) {
		
		Pokemon s = new Pokemon();
		
		
		System.out.println(s.getName());
		s.move();
		s.fight();
		
		System.out.println(s.getName());
		s.move();
		s.fight();
		
		System.out.println(s.getName());
		
	}

}
