package demo;

import adapter.EU_Plug;

public class Demo {
	
	public static void main(String[] args) {
		
		EU_Plug plug = new EU_Plug();
		
		plug.attachToEU("Tietokone");
		
		plug.attachToEUtoUK("Pleikkari");
		
	}

}
