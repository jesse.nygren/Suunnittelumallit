package adapter;

import uk.UK_Plug;
import uk.UK_Socket;

public class EU_UK_Adapter implements EU_Socket {

	private UK_Socket ukSocket;
	
	public EU_UK_Adapter(){
		ukSocket = new UK_Plug();
	}
	
	@Override
	public void attachToEU(String aparaatti) {
		ukSocket.attachToUK(aparaatti);
	}

}
