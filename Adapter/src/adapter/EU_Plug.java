package adapter;

public class EU_Plug implements EU_Socket {

	private EU_UK_Adapter adapter;
	
	@Override
	public void attachToEU(String aparaatti) {
		System.out.println(aparaatti + " attached to EU Socket");
		
	}
	
	
	public void attachToEUtoUK(String aparaatti){
		adapter = new EU_UK_Adapter();
		
		adapter.attachToEU(aparaatti);
	}

}
