package Client;

public class Main {
	
	public static void main(String[] args) {
		
		// Luodaan Jasper-JavaKoodaaja
		Jasper jasper = new Jasper();
		// Luettele vaatteet
		jasper.luettele();
		// Valmistu Metropoliasta
		jasper.valmistu();
		// Luettele uudet vaatteet
		jasper.luettele();
		
	}
	
}
