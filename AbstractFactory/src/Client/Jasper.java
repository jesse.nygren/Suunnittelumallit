package Client;

import Clothes.Cap;
import Clothes.Jeans;
import Clothes.Shoes;
import Clothes.TShirt;
import Factories.AdidasTehdas;
import Factories.BossTehdas;
import Factories.Vaatetehdas;

public class Jasper {

	private Vaatetehdas tehdas = null;

	private boolean valmistunut = false;

	// Luettele vaatteet
	public void luettele() {
		if (valmistunut == false){
			tehdas = new AdidasTehdas();
			System.out.println("Opiskelija-level");
		} else if (valmistunut == true){
			tehdas = new BossTehdas();
			System.out.println("Insin��ri-level");
		}
		System.out.println(tehdas.createJeans() + ", " + tehdas.createTShirt() + ", " + tehdas.createCap() + ", "
				+ tehdas.createShoes());
	}

	// Valmistu Metropoliasta
	public void valmistu() {
		valmistunut = true;
	}

}
