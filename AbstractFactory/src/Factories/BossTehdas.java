package Factories;

import BOSS.BOSSCap;
import BOSS.BOSSJeans;
import BOSS.BOSSShoes;
import BOSS.BOSSTShirt;
import Clothes.Cap;
import Clothes.Jeans;
import Clothes.Shoes;
import Clothes.TShirt;

public class BossTehdas implements Vaatetehdas {

	@Override
	public Jeans createJeans() {

		return new BOSSJeans();
	}

	@Override
	public TShirt createTShirt() {
	
		return new BOSSTShirt();
	}

	@Override
	public Cap createCap() {
	
		return new BOSSCap();
	}

	@Override
	public Shoes createShoes() {
	
		return new BOSSShoes();
	}



}
