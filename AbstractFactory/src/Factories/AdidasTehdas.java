package Factories;

import Adidas.AdidasCap;
import Adidas.AdidasJeans;
import Adidas.AdidasShoes;
import Adidas.AdidasTShirt;
import Clothes.Cap;
import Clothes.Jeans;
import Clothes.Shoes;
import Clothes.TShirt;

public class AdidasTehdas implements Vaatetehdas {

	@Override
	public Jeans createJeans() {
		
		return new AdidasJeans();
	}

	@Override
	public TShirt createTShirt() {
	
		return new AdidasTShirt();
	}

	@Override
	public Cap createCap() {

		return new AdidasCap();
	}

	@Override
	public Shoes createShoes() {
	
		return new AdidasShoes();
	}



}
