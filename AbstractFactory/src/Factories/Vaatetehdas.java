package Factories;

import Clothes.Cap;
import Clothes.Jeans;
import Clothes.Shoes;
import Clothes.TShirt;

public interface Vaatetehdas {

	public abstract Jeans createJeans();
	public abstract TShirt createTShirt();
	public abstract Cap createCap();
	public abstract Shoes createShoes();
}
