package demo;

import command.Command_IF;
import command.ScreenDownCmnd;
import command.ScreenUpCmnd;
import invoker.RemoteController;
import receiver.Screen;

public class User {
	
	public static void main(String[] args) {
		
		Screen screen = new Screen();
		
		Command_IF upCmd = new ScreenUpCmnd(screen);
		Command_IF downCmd = new ScreenDownCmnd(screen);
		RemoteController remoteUpBtn = new RemoteController(upCmd);
		RemoteController remoteDownBtn = new RemoteController(downCmd);
		
		remoteUpBtn.push();
		remoteDownBtn.push();
		
		
	}

}
