package command;

import receiver.Screen;

public class ScreenDownCmnd implements Command_IF{

	Screen screen;
	
	public ScreenDownCmnd(Screen screen) {
		this.screen = screen;
	}
	
	@Override
	public void execute() {
		screen.down();
	}

}
