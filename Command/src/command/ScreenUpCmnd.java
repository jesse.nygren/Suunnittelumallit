package command;

import receiver.Screen;

public class ScreenUpCmnd implements Command_IF {

	Screen screen;
	
	public ScreenUpCmnd(Screen screen) {
		this.screen = screen;
	}
	
	@Override
	public void execute() {
		screen.up();
		
	}

}
