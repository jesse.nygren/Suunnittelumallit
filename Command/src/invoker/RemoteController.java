package invoker;

import command.Command_IF;

public class RemoteController {
	
	Command_IF cmd;
	
	public RemoteController(Command_IF cmd) {
		this.cmd = cmd;
	}
	
	public void push() {
		cmd.execute();
	}

}
