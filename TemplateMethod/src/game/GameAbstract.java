package game;

public abstract class GameAbstract {


	protected int playersCount;
	

	abstract void initializeGame();

	
	abstract void makePlay(int player);

	
	abstract boolean endOfGame();

	
	abstract void printWinner();
	
	public final void playOneGame(int playerCount) {
		this.playersCount = playerCount;
		initializeGame();
		int j = 0;
		while(!endOfGame()) {
			makePlay(j);
			j = (j+1) % playerCount;
		}
		printWinner();
	}
	
}
