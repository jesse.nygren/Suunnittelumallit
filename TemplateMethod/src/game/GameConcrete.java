package game;

import java.util.HashMap;
import java.util.Map;
import java.util.Scanner;

public class GameConcrete extends GameAbstract {

	private int dice;
	private Scanner sc = new Scanner(System.in);
	private int c;
	
	HashMap<Integer, Integer> results = new HashMap<Integer, Integer>();
	
	@Override
	void initializeGame() {
		dice = 0;
		c = 0;
	}

	@Override
	void makePlay(int player) {
		System.out.println("- - - - - - - - - - ");
		System.out.println("Player " + player + " turn!");
		System.out.println("Type 1 & ENTER");
		while (c != 1) {
			c = sc.nextInt();
			if (c != 1) {
				System.out.println("Invalid command!");
			}
		}
		int res = throwDice();
		System.out.println("Dice value: " + res);
		results.put(player, res);
		c = 0;
		

	}

	@Override
	boolean endOfGame() {

		if (results.containsKey(playersCount-1) == true) {
			return true;
		}
	
		return false;
	}

	@Override
	void printWinner() {

		int highestMap = 0;
		int highestRes = 0;
		for (Map.Entry<Integer, Integer> entry : results.entrySet()) {
			if (entry.getValue() > highestRes) {
				highestMap = entry.getKey();
				highestRes = entry.getValue();
			}
		}
		System.out.println();
		System.out.println("!!!!!!!!!!!!!!!!!!!!!!!!!!!!!");
		System.out.println("THE WINNER IS Player: " + highestMap);
		System.out.println("!!!!!!!!!!!!!!!!!!!!!!!!!!!!!");
		
		
	}

	private int throwDice() {
		dice = (int) (Math.random() * 99999) + 1;
		return dice;
	}

}
