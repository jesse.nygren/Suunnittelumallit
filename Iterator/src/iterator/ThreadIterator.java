package iterator;

import java.util.Iterator;

public class ThreadIterator extends Thread{

	
	private Iterator<String> itr;
	private int id;
	
	public ThreadIterator(int id, Iterator<String> itr) {
		this.itr = itr;
		this.id = id;
	}

	public void run() {
		while(this.itr.hasNext()) {
			System.out.println(id + ": " + this.itr.next());
		}
	}

}
