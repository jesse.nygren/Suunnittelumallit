package iterator;

import java.util.ArrayList;
import java.util.Iterator;

public class Demo {

	public static void main(String[] args) {
		
	ArrayList<String> nimet = new ArrayList<String>();
	
	nimet.add("Kapa");
	nimet.add("Repa");
	nimet.add("Pera");
	nimet.add("Jorma");
	nimet.add("Kaitsu");
	nimet.add("Kake");
	nimet.add("Jouni");
	nimet.add("Jonne");
	
//	System.out.println("Kaksi eri iteraattoria");
//	ThreadIterator itr1 = new ThreadIterator(1, nimet.iterator());
//	ThreadIterator itr2 = new ThreadIterator(2, nimet.iterator());
//	itr1.start();
//	itr2.start();
	
	System.out.println("Sama iteraattori kahdella s�ikeell�");
	Iterator i = nimet.iterator();
	ThreadIterator itr3 = new ThreadIterator(1, i);
	ThreadIterator itr4 = new ThreadIterator(2, i);
	itr3.start();
	itr4.start();

	
	
	}
	
}
