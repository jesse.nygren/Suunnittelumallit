package strategy;

import java.util.List;
import java.util.ListIterator;

public class TwoBreakStrat implements ListConverter {

	private ListIterator itr;
	
	@Override
	public String listToString(List<String> list) {
		
		String listString = "";
		
		itr = list.listIterator();
		
		while (itr.hasNext()) {
			listString += itr.next() + "" + itr.next() +"\n";
			
		}
		
		return listString;
	}


}
