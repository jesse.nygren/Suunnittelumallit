package strategy;

import java.util.ArrayList;
import java.util.List;

public class Context {

	private List<String> lista;
	private ListConverter lc;
	

	public Context(ListConverter lc) {
		this.lista = new ArrayList<String>();
		this.lc = lc;
		
		this.lista.add("a");
		this.lista.add("b");
		this.lista.add("c");
		this.lista.add("d");
		this.lista.add("e");
		this.lista.add("f");
		this.lista.add("g");
		this.lista.add("h");
		this.lista.add("i");
		this.lista.add("j");
		this.lista.add("k");
		this.lista.add("l");
		this.lista.add("m");
		this.lista.add("n");
		this.lista.add("o");
		this.lista.add("p");
		this.lista.add("q");
		this.lista.add("s");
		this.lista.add("t");
		this.lista.add("u");
		this.lista.add("v");
		this.lista.add("w");
		this.lista.add("x");
		this.lista.add("y");
		this.lista.add("z");
		this.lista.add("�");
		this.lista.add("�");
		this.lista.add("�");
		
	}
	

	public void printString() {
		System.out.println(lc.listToString(lista));
	}
	
	
}
