package strategy;

import java.util.ArrayList;
import java.util.List;
import java.util.ListIterator;

public class OneBreakStrat implements ListConverter {

	
	@Override
	public String listToString(List<String> list) {
		
		String listString = "";
		
		for (String s : list) {
			listString += s + "\n";
			
		}
		
		return listString;
	}

}
