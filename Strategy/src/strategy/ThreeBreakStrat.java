package strategy;

import java.util.Arrays;
import java.util.List;
import java.util.ListIterator;

public class ThreeBreakStrat implements ListConverter {

	private ListIterator itr;
	
	@Override
	public String listToString(List<String> list) {
		
		String listString = "";

		itr = list.listIterator();
		
		while (itr.hasNext()) {
			listString += itr.next();
			if (itr.hasNext()) {
				listString += itr.next();
				if (itr.hasNext()) {
					listString += itr.next() + "\n";
				}
			}
		
			
		}
		
		
		return listString;
	}

}
