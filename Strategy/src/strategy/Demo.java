package strategy;

public class Demo {
	public static void main(String[] args) {
		
		Context c = new Context(new OneBreakStrat());
		
		c.printString();

		c = new Context(new TwoBreakStrat());
		
		c.printString();
			
		c = new Context(new ThreeBreakStrat());
		
		c.printString();

		
	}
}
